# lxcwrapper
simple create, start and config autostart ubuntu linux container with static ip v4 on the local host

### defaults
- ubuntu lxc user: ubuntu
- id_rsa pub key in the root directory will be copied to the 
  authorised keys file of the default user, if it is not this dummeykey
- nameservers '8.8.8.8, 8.8.4.4'


## requirements
- running lxc system with bridge network <br />
  [hint: '/etc/default/lxc-net', '/etc/lxc/default.conf'] <br />
  (sudo apt-get install lxc, bridge-utils) <br />
  (https://www.itzgeek.com/how-tos/linux/ubuntu-how-tos/configure-bridged-networking-for-kvm-on-ubuntu-14-10.html) 
- wrapper execution with a user with excess to lxc daemon  
- python3 


## install
- python3-lxc 
  sudo apt install python3-lxc
- git clone https://gitlab.com/eheimwbn/lxcwrapper.git<br />
  or download the zip file and unzip it
- copy your deployment pub key to the projects root file system 
- chmod +x ./lxcwarpper
  
  
## execute
./lxcwarpper -n srv -f xenial -a 192.168.1.122/24 -g 192.168.1.1


###### will create a lxc with 

- based on ubuntu xenial
- name: srv
- IPv4: 192.168.1.122
- netmask: 255.255.255.0
- gateway: 192.168.1.1
- set him to autostart

