#!/usr/bin/python3

import lxc
import sys


# Setup the container object
def create_container(name: str, release: str, ipv4: str, gwv4: str, key: str):
    c = lxc.Container(name)
    if c.defined:
        print("Container already exists", file=sys.stderr)
        sys.exit(1)


    # Create the container rootfs
    if not c.create("download", lxc.LXC_CREATE_QUIET, {"dist": "ubuntu",
                                                       "release": release,
                                                       "arch": "amd64"}):
        print("Failed to create the container rootfs", file=sys.stderr)
        sys.exit(1)

    # Start the container
    if not c.start():
        print("Failed to start the container", file=sys.stderr)
        sys.exit(1)

    # Set networking
    c.attach_wait(lxc.attach_run_command, ["sed", "-i", "-e", "s/dhcp/static/g", "/etc/network/interfaces"])
    address = "$ a   address " + ipv4
    c.attach_wait(lxc.attach_run_command, ["sed", "-i", address, "/etc/network/interfaces"])
    gateway = "$ a   gateway " + gwv4
    c.attach_wait(lxc.attach_run_command, ["sed", "-i", gateway, "/etc/network/interfaces"])
    nameservers = "$ a dns-nameservers 8.8.8.8 8.8.4.4"
    c.attach_wait(lxc.attach_run_command, ["sed", "-i", nameservers, "/etc/network/interfaces"])

    # copy the pupkey if not the dummy key
    if 'dummyfile' not in key:
        c.attach_wait(lxc.attach_run_command, ["mkdir", "-p",  "/home/ubuntu/.ssh"])
        c.attach_wait(lxc.attach_run_command, ["touch",  "/home/ubuntu/.ssh/authorized_keys"])
        pkey = 'echo ' + key + ' >> /home/ubuntu/.ssh/authorized_keys'
        c.attach_wait(lxc.attach_run_command, ["sh", "-c", pkey])
        c.attach_wait(lxc.attach_run_command, ["chown", "ubuntu:ubuntu", "/home/ubuntu/.ssh/authorized_keys"])


    else:
        print('Found dummyfile, skipping copy the key')

    c.attach_wait(lxc.attach_run_command, ["touch", "/etc/sudoers.d/user_ubuntu"])

    sudouser_A = 'echo "#Rules for ubuntu user"  >> /etc/sudoers.d/user_ubuntu'
    c.attach_wait(lxc.attach_run_command, ["sh", "-c", sudouser_A])
    sudouser_B = 'echo "ubuntu ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers.d/user_ubuntu'
    c.attach_wait(lxc.attach_run_command, ["sh", "-c", sudouser_B])

    # stop and start lxc tho activate the new network settings
    if not c.stop():
        print("Failed to stop the container", file=sys.stderr)
        sys.exit(1)

    # set lxc to autostart
    c.set_config_item('lxc.start.auto', '1')

    if not c.start():
        print("Failed to start the container", file=sys.stderr)
        sys.exit(1)

    # install openssh-server sleep added to ensure network target ist loaded
    c.attach_wait(lxc.attach_run_command, ["sleep", "2"])
    c.attach_wait(lxc.attach_run_command, ["apt-get", "update"])
    c.attach_wait(lxc.attach_run_command, ["apt-get", "install", "openssh-server", "-y"])
